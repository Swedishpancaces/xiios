// Graphics buffer implementation

// TODO: Do something with namespaces to 
// avoid name collitions.
// So in the assembly we would just
// prefix everything in the namespace
// with the name.
// namespace Gcode;

import Constants.t12;
import StdUtil.t12;
import StdGraph.t12;

import Heap.t12;

public:

const word gcode_nop     = 0;
const word gcode_hlt     = 1;
const word gcode_hlt_rst = 2;
const word gcode_jmp     = 3;

const word gcode_line      = 10;
const word gcode_rectangle = 11;
const word gcode_ellipse   = 12;

const word gcode_fontchar                 = 20;
const word gcode_fontchar_background      = 21;
const word gcode_fontchar_mask            = 22;
const word gcode_fontchar_buffer          = 23;
const word gcode_fontchar_buffer_color    = 24;
const word gcode_fontchar_buffer_color_bg = 25;

const word gcode_true_color_sprite      = 30;
const word gcode_paletted_sprite        = 31;
const word gcode_true_color_sprite_mask = 32;
const word gcode_paletted_sprite_mask   = 33;


const word gcode_nop_inst_size     = 1;
const word gcode_hlt_inst_size     = 1;
const word gcode_hlt_rst_inst_size = 1;
const word gcode_jmp_inst_size     = 3;

struct GraphicsBuffer
{
	dword size;
	dword count;
	*word data;
}

// The goal is this!
//struct AllocLikeProc<T> = $(dword) -> *T;

// AllocLikeProc<GraphicsBuffer> alloc = alloc_graphics_buffer;
// *GraphicsBuffer buffer = alloc(100);

struct Vec2
{
	word x;
	word y;
}
	
struct Rect
{
	word x;
	word y;
	word width;
	word height;
}

struct Line
{
	// TODO: Unions
	// Vec2 start;
	// Vec2 end;
	word x1;
	word y1;
	word x2;
	word y2;
}

// FIXME: use Rect or someting
// because we really want to be able to do:
// grect->x instead of grect->rect.x
struct GcodeRect
{
	word instruction;
	word color;
	word x;
	word y;
	word width;
	word height;
}

struct GcodeFontChar
{
	word instruction;
	word color;
	*word char_addr;
	*word vram_addr;
}

struct GcodeFontCharBackground
{
	word instruction;
	word color;
	word background_color;
	*word char_addr;
	*word vram_addr;
}

struct PalettedSprite
{
	*word sprite;
	*word palette;
	word x;
	word y;
	word stride;
	word width;
	word height;
}

struct PalettedSpriteMask
{
	*word sprite;
	*word palette;
	*word mask;
	word x;
	word y;
	word stride;
	word width;
	word height;
}

// FIXME: Use PalettedSprite struct
// instead of just copying the feilds

// That way when we get the pointer from the buffer
// we can't change the instruction!

struct GcodePalettedSprite
{
	word instruction;
	*word vram_addr;
	*word sprite;
	*word palette;
	word stride;
	word width;
	word height;
}

struct GcodePalettedSpriteMask
{
	word instruction;
	*word vram_addr;
	*word sprite;
	*word palette;
	*word mask;
	word stride;
	word width;
	word height;
}

struct FontcharBuffer
{
	word instruction;
	word color;
	*char char_buffer;
	dword buffer_length;
	*word vram_addr;
	*word font_addr;
}

struct FontcharBufferColor
{
	word instruction;
	*char char_buffer;
	*word color_buffer;
	dword buffer_length;
	*word vram_addr;
	*word font_addr;
}

struct FontcharBufferColorBackground
{
	word instruction;
	*char char_buffer;
	*word color_buffer;
	*word background_color_buffer;
	dword buffer_length;
	*word vram_addr;
	*word font_addr;
}

// TODO: Free GraphicsBuffer

void init_graphics_buffer(*GraphicsBuffer buffer, dword initialSize)
{
	buffer->size = initialSize;
	buffer->count = 0;
	buffer->data = cast(*word) alloc_w2(initialSize);
}

*GraphicsBuffer alloc_graphics_buffer(dword initialSize)
{
	*GraphicsBuffer buffer = alloc_struct<GraphicsBuffer>();
	
	init_graphics_buffer(buffer, initialSize);
	
	return buffer;
}

void free_graphics_buffer(*GraphicsBuffer buffer)
{
	free(buffer->data);
	free(buffer);
}

void ensure_capacity(*GraphicsBuffer buffer, dword capacity)
{
	if (buffer->size < capacity)
	{
		grow(buffer, capacity);
	}
}

void grow(*GraphicsBuffer buffer, dword size)
{
	dword newSize = buffer->size + (buffer->size / 2);
	
	if (newSize < size) newSize = size;
	
	if (newSize > gram_size)
		panic_string("GCODE_BUFFER_OVERFLOW");
	
	*word oldArray = buffer->data;
	
	*word newArray = cast(*word) alloc_w2(newSize);
	
	// Copy over the new data
	memcpy(oldArray, newArray, buffer->size);
	
	// Set the array first
	// to avoid ever putting the struct into an invalid state
	buffer->data = newArray;
	buffer->size = newSize;
	
	free(oldArray);
}

void add_graphics_bufffer(*GraphicsBuffer toBuffer, *GraphicsBuffer fromBuffer)
{
	ensure_capacity(toBuffer, toBuffer->count + fromBuffer->count);
	
	memcpy(fromBuffer->data, &toBuffer->data[toBuffer->count], fromBuffer->count);
	
	toBuffer->count += fromBuffer->count;
}

void add_hlt(*GraphicsBuffer buffer)
{
	ensure_capacity(buffer, buffer->count + gcode_hlt_inst_size);
	
	*word data = buffer->data;
	
	data[buffer->count] = gcode_hlt;
	
	buffer->count += gcode_hlt_inst_size;
}

void add_hlt_rst(*GraphicsBuffer buffer)
{
	ensure_capacity(buffer, buffer->count + gcode_hlt_rst_inst_size);
	
	*word data = buffer->data;
	
	data[buffer->count] = gcode_hlt_rst;
	
	buffer->count += gcode_hlt_rst_inst_size;
}

void remove_hlt_rst(*GraphicsBuffer buffer)
{
	if (buffer->data[buffer->count - 1] == gcode_hlt_rst)
		buffer->count--;
	else panic_string("GBUFFER_REMOVE_INST_WAS_NOT_HLT_RST");
}

dword add_rect(*GraphicsBuffer buffer, word color, word x, word y, word width, word height)
{
	ensure_capacity(buffer, buffer->count + sizeof(GcodeRect));
	
	dword count = buffer->count;
	*GcodeRect data = 
		cast(*GcodeRect) &buffer->data[count];
	
	data->instruction = gcode_rectangle;
	data->color = color;
	data->x = x;
	data->y = y;
	data->width = width;
	data->height = height;
	
	buffer->count += sizeof(GcodeRect);
	
	// Return the buffer offset that this instruction has
	return count;
}

*GcodeRect rect_from_offset(*GraphicsBuffer buffer, dword index)
{
	return cast(*GcodeRect) &buffer->data[index];
}

dword add_fontchar(*GraphicsBuffer buffer, word color, *word char_addr, *word vram_addr)
{
	ensure_capacity(buffer, buffer->count + sizeof(GcodeFontChar));
	
	dword count = buffer->count;
	*GcodeFontChar data = 
		cast(*GcodeFontChar) &buffer->data[count];
	
	data->instruction = gcode_fontchar;
	data->color = color;
	data->char_addr = char_addr;
	data->vram_addr = vram_addr;
	
	buffer->count += sizeof(GcodeFontChar);
	
	// Return the buffer offset that this instruction has
	return count;
}

dword add_fontchar_background(*GraphicsBuffer buffer, word color, word bg_color, *word char_addr, *word vram_addr)
{
	ensure_capacity(buffer, buffer->count + sizeof(GcodeFontCharBackground));
	
	dword count = buffer->count;
	*GcodeFontCharBackground data = 
		cast(*GcodeFontCharBackground) &buffer->data[count];
	
	data->instruction = gcode_fontchar_background;
	data->color = color;
	data->char_addr = char_addr;
	data->vram_addr = vram_addr;
	
	buffer->count += sizeof(GcodeFontCharBackground);
	
	// Return the buffer offset that this instruction has
	return count;
}

dword add_ps_sprite(*GraphicsBuffer buffer, PalettedSprite sprite)
{
	ensure_capacity(buffer, buffer->count + sizeof(GcodePalettedSprite));
	
	dword count = buffer->count;
	*GcodePalettedSprite data =
		cast(*GcodePalettedSprite) &buffer->data[count];
	
	if (sprite.stride % 3 != 0) panic_string("STRIDE_NOT_MULTIPLE_OF_THREE");
	
	data->instruction = gcode_paletted_sprite_mask;
	data->vram_addr = calc_vram_addr(sprite.x, sprite.y);
	data->sprite = sprite.sprite;
	data->palette = sprite.palette;
	data->stride = sprite.stride;
	data->width = sprite.width;
	data->height = sprite.height;
	
	buffer->count += sizeof(GcodePalettedSprite);
	
	// Return the buffer offset that this instruction has
	return count;
}

dword add_ps_sprite_mask(*GraphicsBuffer buffer, PalettedSpriteMask sprite)
{
	ensure_capacity(buffer, buffer->count + sizeof(GcodePalettedSpriteMask));
	
	dword count = buffer->count;
	*GcodePalettedSpriteMask data =
		cast(*GcodePalettedSpriteMask) &buffer->data[count];
	
	if (sprite.stride % 3 != 0) panic_string("STRIDE_NOT_MULTIPLE_OF_THREE");
	
	data->instruction = gcode_paletted_sprite_mask;
	data->vram_addr = calc_vram_addr(sprite.x, sprite.y);
	data->sprite = sprite.sprite;
	data->palette = sprite.palette;
	data->mask = sprite.mask;
	data->stride = sprite.stride;
	data->width = sprite.width;
	data->height = sprite.height;
	
	buffer->count += sizeof(GcodePalettedSpriteMask);
	
	// Return the buffer offset that this instruction has
	return count;
}

dword add_fontchar_buffer(*GraphicsBuffer buffer, word color, *char char_buffer, dword buffer_length, *word vram_addr, *word font_addr)
{
	ensure_capacity(buffer, buffer->count + sizeof(FontcharBuffer));
	
	dword count = buffer->count;
	*FontcharBuffer data = 
		cast(*FontcharBuffer) &buffer->data[count];
	
	data->instruction = gcode_fontchar_buffer;
	data->color = color;
	data->char_buffer = char_buffer;
	data->buffer_length = buffer_length;
	data->vram_addr = vram_addr;
	data->font_addr = font_addr;
	
	buffer->count += sizeof(FontcharBuffer);
	
	// Return the buffer offset that this instruction has
	return count;
}

*FontcharBuffer fontchar_buffer_from_index(*GraphicsBuffer buffer, dword index)
{
	return cast(*FontcharBuffer) &buffer->data[index];
}

dword add_fontchar_buffer_color(*GraphicsBuffer buffer, *char char_buffer, *word color_buffer, dword buffer_length, *word vram_addr, *word font_addr)
{
	ensure_capacity(buffer, buffer->count + sizeof(FontcharBufferColor));
	
	dword count = buffer->count;
	*FontcharBufferColor gcode_buffer = 
		cast(*FontcharBufferColor) &buffer->data[count];
	
	gcode_buffer->instruction = gcode_fontchar_buffer_color;
	gcode_buffer->char_buffer = char_buffer;
	gcode_buffer->color_buffer = color_buffer;
	gcode_buffer->buffer_length = buffer_length;
	gcode_buffer->vram_addr = vram_addr;
	gcode_buffer->font_addr = font_addr;
	
	buffer->count += sizeof(FontcharBufferColor);
	
	// Return the buffer offset that this instruction has
	return count;
}

dword add_fontchar_buffer_color_background(*GraphicsBuffer buffer, *char char_buffer, *word color_buffer, *word background_color_buffer, dword buffer_length, *word vram_addr, *word font_addr)
{
	ensure_capacity(buffer, buffer->count + sizeof(FontcharBufferColorBackground));
	
	dword count = buffer->count;
	*FontcharBufferColorBackground gcode_buffer = 
		cast(*FontcharBufferColorBackground) &buffer->data[count];
	
	gcode_buffer->instruction = gcode_fontchar_buffer_color_bg;
	gcode_buffer->char_buffer = char_buffer;
	gcode_buffer->color_buffer = color_buffer;
	gcode_buffer->background_color_buffer = background_color_buffer ;
	gcode_buffer->buffer_length = buffer_length;
	gcode_buffer->vram_addr = vram_addr;
	gcode_buffer->font_addr = font_addr;
	
	buffer->count += sizeof(FontcharBufferColorBackground);
	
	// Return the buffer offset that this instruction has
	return count;
}

*T gcode_struct_from_offset<T>(*GraphicsBuffer buffer, dword index)
{
	return cast(*T) &buffer->data[index];
}

dword add_gcode_struct<T>(*GraphicsBuffer buffer, *T gcode_struct)
{
	ensure_capacity(buffer, buffer->count + sizeof(T));
	
	dword count = buffer->count;
	
	memcpy(gcode_struct, &buffer->data[count], sizeof(T));
	
	buffer->count += sizeof(T);
	
	return count;
}






