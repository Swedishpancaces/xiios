
import StdCallback.t12;
import Console.t12;

import List.t12;

import MessageQueue.t12;

import StdMath.t12;
import StdUtil.t12;

import StdGraph.t12;

// FIXME: We need to include this so that the generic generation
// for the list types to work
// At the moment those procs are emitted in the scope of the using
// function, so if the generic function requires an import, so do we.
import Heap.t12;

public:

global word mouse_pos_x;
global word mouse_pos_y;

private:

// From Cursor.12asm
extern const word cursor_width;
extern const word cursor_height;

const word mouse_button_callback_args   = 1; // state w
const word mouse_buttons_callback_args  = 2; // button w, state w
const word mouse_scroll_callback_args   = 1; // scroll w

global word mouse_buttons;

struct MouseButtonCallback = $(word, word);
struct MouseScrollCallback = $(word);

global List<MouseButtonCallback> mouse_button_callback;
global List<MouseScrollCallback> mouse_scroll_callback;

// The 5 most significant bits are the mouse buttons
// The 6th msb is reserved for future expansion
const word mouse_left_button    = 0b000010000000;
const word mouse_right_button   = 0b000100000000;
const word mouse_middle_button  = 0b001000000000;
const word mouse_back_button    = 0b010000000000;
const word mouse_forward_button = 0b100000000000;

const word mouse_button_mask    = 0b111110000000;
const word mouse_scroll_mask    = 0b000000111111;

const word scroll_sign_bit      = 0b000000100000;
const word scroll_sign_extend   = 0b111111000000;

void init_mouse()
{
	init_list<MouseButtonCallback>(&mouse_button_callback, HEAP_BLOCK_SIZE / 2);
	init_list<MouseScrollCallback>(&mouse_scroll_callback, HEAP_BLOCK_SIZE / 2);
}

interrupt mouse(word x, word y, word status)
{
	// TODO: Don't remove the cursor width and height here!
	word mouse_pos_x = clamp(x, 0, screen_width - cursor_width);
	word mouse_pos_y = clamp(y, 0, screen_height - cursor_height);
	
	disable_interrupts();
	post_message(MOUSE_MOVED_MSG, mouse_pos_x * 0x1_000 | mouse_pos_y);
	enable_interrupts();
	
	update_mouse_buttons(status & mouse_button_mask);
	
	if((status & mouse_scroll_mask) != 0)
	{
		// There is some scroll!
		
		// FIXME: This is what we want to do!!!
		// We shift up the sign bit and then shift down
		// to let sign extension do it's thing
		// word dScroll = (((status & mouse_scroll_mask) << 6) >> 6);
		
		word dScroll = (status & mouse_scroll_mask) | 
					((status & scroll_sign_bit) != 0 ? scroll_sign_extend : 0);
		
		//change_line_offset(dScroll);
		
		disable_interrupts();
		post_message(MOUSE_SCROLLED_MSG, dScroll);
		enable_interrupts();
		
		// TODO: Call scroll callback!
		// trigger_callback(mouse_scroll_callback, &dScroll);
	}
}

void update_mouse_buttons(word buttons)
{
	word last = mouse_buttons;
	mouse_buttons = buttons;
	
	word changed = buttons ^ last;
	
	// FIXME: Check mouse presses!!
}

void register_mouse_button_callback(MouseButtonCallback listener)
{
	list_add<MouseButtonCallback>(&mouse_button_callback, listener);
}

void register_mouse_scroll_callback(MouseScrollCallback listener)
{
	list_add<MouseScrollCallback>(&mouse_scroll_callback, listener);
}

void unregister_mouse_button_callback(MouseButtonCallback listener)
{
	list_remove_element<MouseButtonCallback>(&mouse_button_callback, listener);
}

void unregister_mouse_scroll_callback(MouseScrollCallback listener)
{
	list_remove_element<MouseScrollCallback>(&mouse_scroll_callback, listener);
}

bool mouse_left_down()
{
	return mouse_buttons & mouse_left_button;
}

bool mouse_right_down()
{
	return mouse_buttons & mouse_right_button;
}

bool mouse_middle_down()
{
	return mouse_buttons & mouse_middle_button;
}

bool mouse_back_down()
{
	return mouse_buttons & mouse_back_button;
}

bool mouse_forward_down()
{
	return mouse_buttons & mouse_forward_button;
}






