; Standard string library for VM12

; String format in VM12 is the length of the string in two words
; followed by that number of ascii characters

; There are also "raw" strings that do not contain their length
; These are used in already managed spaces such as the console buffer

!private

<true	= 1>
<false	= 0>

:copy_string_alloc
	2 2				; (/src w*)
	loadl 0			; [src]
	loadl 0			; [src]
	loadl [SP]		; length
	::alloc_w2
	::copy_string
	ret2

:copy_string
	4 4				; (/src w*, /dest w*)
	loadl 0			; [src]
	loadl 2			; [dest]
	loadl 0			; [src]
	loadl [SP]		; length
	linc linc		; += 2
	memc
	loadl 2			; [dest]
	ret2

:raw_to_safe
	4 6				; (/str w*, /length w2), safe_str w*
	loadl 2			; [length]
	linc linc
	::alloc_w2
	storel 4		; [safe_str]
	
	loadl 4			; [safe_str]
	loadl 2			; [length]
	storel [SP]
	
	loadl 0			; [str]
	loadl 4			; [safe_str]
	loadl #2
	ladd
	loadl 2			; [length]
	memc
	
	loadl 4			; [safe_str]
	ret2			; safe_str
	
:string_equals
	4 6				; (/str1 w*, /str2 w*) counter w2
	loadl 0			; [str1]
	loadl [SP]		; str1->length
	loadl 2			; [str2]
	loadl [SP]		; str2->length
	jneql :not_equal
	
	;derefl(0)		; str1->length
	loadl 0			; [str1]
	loadl [SP]		; str1->length
	storel 4		; [counter]
	
	linc 0			; str1 += 2
	linc 0
	
	linc 2			; str2 += 2
	linc 2
	
	; Go through all characters
	:loop
	loadl 4			; [counter]
	jzl :loop_end
	
	loadl 0			; [str1]
	load [SP]
	
	loadl 2			; [str2]
	load [SP]
	
	jneq :not_equal
	
	linc 0			; [str1]
	linc 2			; [str2]
	ldec 4			; [counter]
	jmp :loop
	:loop_end
	
	load #true
	ret1
	:not_equal
	load #false
	ret1
	
:concat_string_alloc
	4 4				; (/str1 w*, /str2 w*)
	loadl 0			; [str1]
	loadl 2			; [str2]
	loadl 0			; [str1]
	loadl [SP]		; [str1->length]
	loadl 2			; [str2]
	loadl [SP]		; [str2->length]
	ladd
	loadl #2		; length part
	ladd
	::alloc_w2
	::concat_string
	ret2

:concat_string_alloc_and_free
	4 4				; (/str1 w*, /str2 w*)
	loadl 0			; [str1]
	loadl 2			; [str2]
	loadl 0			; [str1]
	loadl [SP]		; [str1->length]
	loadl 2			; [str2]
	loadl [SP]		; [str2->length]
	ladd
	loadl #2		; length part
	ladd
	::alloc_w2
	::concat_string
	
	loadl 0			; [str1]
	::free
	loadl 2			; [str2]
	::free
	
	ret2
	
; FIXME: The alloc version should be default
:concat_string
	6 10			; (/str1 w*, /str2 w*, /str_res w*), length_1 w2, length_2 w2
	loadl 0			; [str1]
	loadl [SP]
	storel 6		; [length_1]
	loadl 2			; [str2]
	loadl [SP]
	storel 8		; [length_2]
	
	loadl 4			; [str_res]
	loadl 6			; [length_1]
	loadl 8			; [length_2]
	ladd
	storel [SP]
	
	loadl 0			; [str1]
	loadl #2
	ladd
	
	loadl 4			; [str_res]
	loadl #2
	ladd
	
	loadl 6			; [length_1]
	
	memc
	
	loadl 2			; [str2]
	loadl #2
	ladd
	
	loadl 4			; [str_res]
	loadl #2
	ladd
	loadl 6			; [length_1]
	ladd
	
	loadl 8			; [length_2]
	
	memc
	
	loadl 4			; [str_res]
	ret2

:substring
	4 6				; (/str w*, /start_index w2), length w2
	; Get length
	loadl 0			; [str]
	loadl [SP]
	storel 4		; [length]
	; Start of str data
	loadl 0			; [str]
	loadl #2
	loadl 2			; [start_index]
	ladd
	ladd
	; New_str length
	loadl 4			; [length]
	loadl 2			; [start_index]
	lsub
	storel 4		; [length]
	; Alloc new string
	loadl 4			; [length]
	loadl #2
	ladd
	::alloc_w2
	ldup
	storel 0		; [str]
	; Store new_length in new_str
	ldup
	loadl 4			; [length]
	storel [SP]
	loadl #2
	ladd
	loadl 4			; [length]
	; Copy data
	memc
	; Return new_str
	loadl 0			; [str]
	ret2
	
:substring_st_le
	6 6				; (/str w*, /start w2, /length w2)
	; Start of str data
	loadl 0			; [str]
	loadl #2
	loadl 2			; [start_index]
	ladd
	ladd
	; Alloc new string
	loadl 4			; [length]
	loadl #2
	ladd
	::alloc_w2
	ldup
	storel 0		; [str]
	; Store new_length in new_str
	ldup
	loadl 4			; [length]
	storel [SP]
	loadl #2
	ladd
	loadl 4			; [length]
	; Copy data
	memc
	; Return new_str
	loadl 0			; [str]
	ret2

:count_same_char_from
	5 9				; (/str *, /char w, /start w2), length w2, count w2
	loadl 0			; [string]
	loadl [SP]
	storel 5		; [length]
	
	; if start > length
	loadl 5			; [length]
	loadl 3			; [start]
	lsub
	jlel :done
	
	loadl #0
	storel 7		; [count]
	
	linc 0			; [str]
	linc 0			; [str]
	
	loadl 0			; [str]
	loadl 3			; [start]
	ladd
	storel 0		; [str]
	
	:loop
	  loadl 0		; [str]
	  load [SP]
	  load 2		; [char]
	  jneq :done
	  
	  linc 7		; [count]
	  linc 0		; [str]
	  loadl 7		; [count]
	  loadl 3		; [start]
	  ladd
	  loadl 5		; [length]
	  jeql :done
	jmp :loop
	:done
	
	loadl 7			; [count]
	ret2
	
	:end_of_string
	loadl #0
	ret2
	
:index_of
	3 3				; (/str w*, /char w), length w2, index w2
	loadl 0			; [str]
	load 2			; [char]
	loadl #0
	::index_of_from
	ret2
	
:index_of_from
	5 9				; (/str w*, /char w, /start w2), length w2, index w2
	loadl 0			; [str]
	loadl [SP]
	storel 5		; [length]
	
	linc 0
	linc 0
	
	; if start > length
	loadl 5			; [length]
	loadl 3			; [start]
	lsub
	jlel :end_of_string
	
	loadl 0			; [str]
	loadl 3			; [start]
	ladd
	storel 0		; [str]
	
	loadl 3			; [start]
	storel 7		; [index]
	
	:loop
	  loadl 0		; [str]
	  load [SP]
	  load 2		; [char]
	  sub
	  jz :found
	  linc 7		; [index]
	  linc 0		; [str]
	  loadl 7		; [index]
	  loadl 5		; [length]
	  lsub
	  jzl :end_of_string
	jmp :loop
	  
	:end_of_string
	loadl #0xFFF_FFF ; Should be #-1
	ret2
	  
	:found
	loadl 7			; [index]
	ret2

:trim_start
	2 8				; (/str w*), length w2, counter w2, new_string w*
	loadl 0			; [str]
	ldup
	loadl #2
	ladd
	storel 0		; [str]
	loadl [SP]
	storel 2		; [length]
	loadl #0
	storel 4		; [counter]
	
	:whitespace_loop
	  
	  loadl 2		; [length]
	  loadl 4		; [counter]
	  lsub
	  jzl :ret_null
	  
	  loadl 0		; [str]
	  load [SP]
	  ::is_whitespace
	  jz :ret_trim
	  
	  linc 0		; [str]
	  linc 4		; [counter]
	jmp :whitespace_loop
	
	:ret_trim
	
	; Calc new length
	loadl 2			; [length]
	loadl 4			; [couter]
	lsub
	storel 2		; [length]
	
	; Allocate the space needed for the new array
	loadl 2			; [length]
	loadl #2
	ladd
	::alloc_w2
	storel 6		; [new_string]
	
	; Set the length of new_string
	loadl 6			; [new_string]
	loadl 2			; [length]
	storel [SP]
	
	; Copy string data
	loadl 0			; [str]
	loadl 6			; [new_string]
	loadl #2
	ladd
	loadl 2			; [length]
	memc
	
	; Return new_string
	loadl 6			; [new_string]
	ret2
	
	:ret_null
	loadl #0
	ret2
	
:trim_end_free_old
	2 2				; (/str *)
	loadl 0
	::trim_end
	loadl 0
	::free
	ret2
	
:trim_end
	2 8				; (/str w*), length w2, counter w2, new_string w*
	loadl 0			; [str]
	loadl [SP]
	storel 2		; [length]
	loadl #0
	storel 4		; [counter]
	linc 0			; [str]
	linc 0			; [str]
	
	:loop
	  loadl 2		; [length]
	  loadl 4		; [counter]
	  jeql :ret_null
	  
	  loadl 0		; [str]
	  loadl 2		; [length]
	  loadl 4		; [counter]
	  lsub
	  ldec
	  ladd
	  load [SP]
	  ::is_whitespace
	  jz :ret_trim
	  
	  linc 4		; [counter]
	jmp :loop
	
	:ret_trim
	
	; Calc new length
	loadl 2			; [length]
	loadl 4			; [counter]
	lsub
	storel 2		; [length]
	
	; Allocate the new array
	loadl 2			; [length]
	loadl #2
	ladd
	::alloc_w
	storel 6		; [new_string]
	
	loadl 6			; [new_string]
	loadl 2			; [length]
	storel [SP]
	
	; Copy string data
	loadl 0			; [str]
	loadl 6			; [new_string]
	loadl #2
	ladd
	loadl 2			; [length]
	memc
	
	loadl 6			; [new_string]
	ret2
	
	:ret_null
	loadl #0
	ret2
	
:count_char
	3 7				; (/str w*, /char w), length w2, count w2
	loadl 0
	loadl [SP]
	storel 3		; [length]
	
	loadl #0
	storel 5		; [count]
	
	:loop
	loadl 3			; [length]
	jzl :end
	
	loadl 0
	loadl #1
	ladd
	loadl 3
	ladd
	load [SP]
	
	load 2			; [char]
	jneq :not_eq
	
	linc 5			; [count]
	
	:not_eq
	ldec 3			; [length]
	jmp :loop
	:end
	loadl 5			; [count]
	ret2
	
:split_string
	3 7				; (/str w*, /char w), list *, curr_index w2
	; Detect how many strings to create?
	; or do it procedurally?
	; Count a counter until we find the char?
	; Use index of to do the fast?
	; Sounds like a good idea
	loadl 0			; [str]
	loadl [SP]
	jzl :empty_array
	
	loadl 0			; [str]
	load 2			; [char]
	::count_char
	linc
	::alloc_list
	storel 3		; [list]
	
	loadl #0
	storel 5		; [curr_index]
	
	:loop
	  loadl 3		; [list]
	  loadl 0		; [str]
	  loadl 5		; [curr_index]
	  
	  loadl 0		; [str]
	  load 2		; [char]
	  loadl 5		; [curr_index]
	  ::index_of_from
	  ldup
	  jlzl :done
	  
	  ldup
	  linc
	  storel 5		; [curr_index]
	  
	  lover
	  lsub
	  
	  ::substring_st_le
	  ::list_add
	  
	  jmp :loop
	:done
	pop pop
	
	loadl 0
	loadl [SP]
	lover
	lsub
	
	::substring_st_le
	::list_add
	
	loadl 3			; [list]
	ldup
	::list_to_array
	lswap
	::free_list
	
	ret2
	
	:empty_array
	load #1
	::alloc
	ldup
	loadl #0
	storel [SP]
	ret2
	
	
:split_string_ignore_empty
	3 7				; (/str w*, /char w), list *, curr_index w2
	loadl 0			; [str]
	loadl [SP]
	jzl :empty_array
	
	loadl 0			; [str]
	load 2			; [char]
	::count_char
	linc
	::alloc_list
	storel 3		; [list]
	
	loadl #0
	storel 5		; [curr_index]
	
	:loop
	  loadl 3		; [list]
	  loadl 0		; [str]
	  loadl 5		; [curr_index]
	  
	  loadl 0		; [str]
	  load 2		; [char]
	  loadl 5		; [curr_index]
	  ::index_of_from
	  ldup
	  jlzl :done
	  
	  ldup
	  linc
	  storel 5		; [curr_index]
	  
	  lover			; calc length
	  lsub
	  
	  ldup
	  jzl :zero_length_string
	  
	  ::substring_st_le
	  ::list_add
	  jmp :loop
	  
	  :zero_length_string
	  
	  pop pop pop pop
	  
	  jmp :loop
	:done
	pop pop
	
	loadl 0
	loadl [SP]
	lover
	lsub
	
	ldup
	jzl :skip_last_part_of_string
	
	::substring_st_le
	::list_add
	
	:skip_last_part_of_string
	
	loadl 3			; [list]
	ldup
	::list_to_array
	lswap
	::free_list
	
	ret2
	
	:empty_array
	load #1
	::alloc
	ldup
	loadl #0
	storel [SP]
	ret2
	

#def str_length(str)
	loadl str loadl [SP]
#end str_length

#def str_char_at(str, index)
	loadl str loadl #2 ladd loadl index ladd load [SP]
#end str_char_at
