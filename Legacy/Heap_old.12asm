; Basic StackHeap implementation

; Struct explanations:
;
; w[32768] metadata
; w[32768][64] heap
;

!global

<HEAP_SIZE			= 32768>
<HEAP_BLOCK_SIZE	= 64>

<heap			= auto(#(#HEAP_SIZE #HEAP_BLOCK_SIZE *))>	; Autos are always sequential
<heap_metadata	= auto(#(#HEAP_SIZE 2 *))>					; They grow down in address space

!private

<metadata = extern(#heap_metadata)>

<ZERO_BLOCK		= auto(#HEAP_BLOCK_SIZE)>

<NULL = extern>

; FIXME: For future robustness there should be some locking mechanism
; So that this api is thread safe

; FIXME: Panic when no memory is available

; TODO: Allocate long living blocks at the end of memory

!private

;#def words_to_blocks(__words)
;	loadl __words loadl #HEAP_BLOCK_SIZE ldec ladd loadl #HEAP_BLOCK_SIZE ldiv
;vis#end words_to_blocks

:init_heap
	0 0
	loadl #metadata
	loadl #HEAP_SIZE
	loadl #0
	::memset_w2
	ret

:calloc_w
	1 1					; (/words w)
	load #0
	load 0
	::calloc_w2
	ret2
	
:calloc_w2
	2 2					; (/words w2)
	;#def words_to_blocks(0)
	loadl 0 loadl #HEAP_BLOCK_SIZE ldec ladd loadl #HEAP_BLOCK_SIZE ldiv
	;#end words_to_blocks
	::calloc_2
	ret2

:calloc
	1 1					; (/size w)
	load #0
	load 0
	::calloc_2
	ret2
	
:calloc_2
	2 2					; (/size w2)
	loadl 0				; [size]
	::alloc_2
	ldup
	loadl 0				; [size]
	loadl #HEAP_BLOCK_SIZE
	lmul
	load #0
	::memset
	ret2
	
:alloc_w
	1 1					; (/words w)
	load #0
	load 0
	::alloc_w2
	ret2
	
:alloc_w2
	2 2					; (/words w2)
	;#def words_to_blocks(0)
	loadl 0 loadl #HEAP_BLOCK_SIZE ldec ladd loadl #HEAP_BLOCK_SIZE ldiv
	;#end words_to_blocks
	::alloc_2
	ret2
	
:alloc
	1 1					; (/size w)
	load #0
	load 0
	::alloc_2
	ret2
	
:alloc_2
	2 6					; (/size w2), meta w2*, counter w2
	loadl 0				; [size]
	jzl :null
	
	load #metadata
	storel 2			; [meta]
	
	:find_empty
	
	; while (*meta != 0)
	:not_found
	loadl 2				; [meta*]
	loadl [SP]			; [meta]
	jzl :found
	
	linc 2				; meta += 2
	linc 2
	
	jmp :not_found
	:found
	
	; counter = 1
	loadl #1
	storel 4			; [counter]
	
	; think of size and counter as unsigned
	; while(counter < size)
	:not_done
	loadl 0				; [size]
	loadl 4				; [counter]
	lsub				; size - counter
	jzl :done			; size == counter (effectively =>)
	
	linc 2				; meta += 2
	linc 2
	
	; if (*meta == 0)
	loadl 2				; [meta]
	loadl [SP]			; *meta
	jnzl :find_empty	; else :find_empty
	
	linc 4				; counter++
	
	jmp :not_done
	:done
	
	; while(counter > 0)
	:write
	loadl 4				; counter
	jzl :return
	
	loadl 2				; [meta]
	loadl 4				; [counter]
	storel [SP]			; *meta = counter
	
	ldec 4				; counter--
	
	ldec 2				; meta -= 2
	ldec 2
	
	jmp :write
	:return
	
	linc 2				; meta += 2
	linc 2
	
	loadl 2				; [meta]
	loadl #metadata
	lsub				; (meta - metadata_start)
	
	ccl rtcr			; (meta - metadata_start) / 2
	
	loadl #HEAP_BLOCK_SIZE
	lmul				; ((meta - metadata_start) / 2) * BLOCK_SIZE
	
	loadl #heap
	ladd				; heap_start + ((meta - metadata_start) / 2) * BLOCK_SIZE
	
	ret2				; return heap_start + (((meta - metadata_start) / 2) * BLOCK_SIZE)
	
	:null
	loadl #NULL
	ret2
	
:free
	2 2					; (/location w*)
	loadl 0				; [location]
	jzl :null
	
	; Check to see the the pointer is on the heap
	; If the pointer is on the heap the carry flag will be set
	;#def lconst_bound(0, #heap, #heap.end)
	ccl loadl 0 loadl #heap lsub loadl #(#heap.end #heap - 1 +) lsub pop pop
	;#end lconst_bound
	jcz :outside_heap
	
	loadl #metadata
	loadl 0				; [location]
	loadl #heap
	lsub
	loadl #HEAP_BLOCK_SIZE
	ldiv
	loadl #2
	lmul
	ladd
	storel 0			; [location]
	
	loadl 0				; [location]
	loadl [SP]
	jzl :not_allocated
	
	:not_at_base
	loadl 0				; [location]
	loadl [SP]
	
	loadl #1
	lsub
	
	jzl :at_base
	
	ldec 0				; [location]
	ldec 0				; [location]
	
	jmp :not_at_base
	:at_base
	
	:not_done
	loadl 0				; [location]
	loadl #0
	storel [SP]
	
	linc 0
	linc 0
	
	loadl 0
	loadl [SP]
	
	ldup
	
	loadl #1
	lsub
	
	jzl :done
	jzl :done
	jmp :not_done
	
	:done
	ret
	
	:outside_heap
	load "FREE_ADDRESS_OUTSIDE_OF_HEAP: 0x"
	loadl 0				; [location]
	::w2_to_padded_hex_string_alloc
	::concat_string_alloc_string_string
	::panic_string
	ret
	:not_allocated
	load "CANNOT_FREE_UNALLOCATED_MEMORY: 0x"
	loadl 0				; [location]
	::w2_to_padded_hex_string_alloc
	::concat_string_alloc_string_string
	::panic_string
	ret
	:null
	; FIXME: Decide if free should accept null pointers
	ret

:free_if_not_null
	2 2					; (/location w*)
	loadl 0				; [location]
	jzl :null
	loadl 0				; [location]
	::free
	:null
	ret
