; Directory inode implementation

; Directory inode structure

; 0 1w	- Type
; 1 1w	- Attrib
; 2 2w	- Parent dir
; 4 14w	- Name
;18 44w	- Inode store (22 inodes)
;62 2w	- Cont. node

!global

<dir_type_offset		= 0>
<dir_attrib_offset		= 1>
<dir_parent_dir_offset	= 2>
<dir_name_offset		= 4>
<dir_inode_store_offset	= 18>
<dir_cont_node_offset	= 62>

<dir_name_length		= 14>
<dir_inode_store_length	= 44>

!private

<inode_type_super		= extern>
<inode_type_directory	= extern>

<inode_size				= extern>

<true	= extern>
<false	= extern>

; TODO: Take cont. nodes into consideration

:get_parent_directory
	2 2				; (/dir_ref w2)
	loadl 0			; [dir_ref]
	::is_directory
	jz :not_directory
	loadl 0			; [dir_ref]
	::load_inode_alloc
	ldup
	loadl #dir_parent_dir_offset
	ladd
	loadl [SP]
	lswap
	::free
	ret2
	:not_directory
	; TODO: Explicitly only allow super
	loadl 0			; [dir_ref]
	ret2

; This proc is almost the same as :cont_node_count_child_nodes
:dir_count_child_nodes
	2 6				; (/dir_data *), count w2, orig_data w*
	loadl #0
	storel 2		; [count]
	
	loadl 0			; [dir_data]
	storel 4		; [orig_data]
	
	loadl 0			; [dir_data]
	loadl #dir_inode_store_offset
	ladd
	storel 0		; [dir_data]
	
	:loop
	  loadl 0		; [dir_data]
	  loadl [SP]
	  jzl :done
	  linc 0		; [dir_data]
	  linc 0		; [dir_data]
	  linc 2		; [count]
	  loadl 2		; [count]
	  lx2()
	  loadl #dir_inode_store_length
	  jeql :done
	jmp :loop
	:done
	
	loadl 4			; [orig_data]
	::inode_data_has_cont_node
	jz :return
	
	derefl(4, #dir_cont_node_offset)
	::load_inode_alloc
	ldup
	::cont_node_count_child_nodes
	lswap
	::free
	
	loadl 2			; [count]
	ladd
	storel 2		; [count]
	
	:return
	loadl 2			; [count]
	ret2
	
:get_dir_child_nodes_alloc
	2 6				; (/inode_ref w2), list *, subinode w2
	loadl 0			; [inode_ref]
	::load_inode_alloc
	storel 0		; [inode_ref] -> [inode_data]
	
	loadl 0			; [inode_data]
	::dir_count_child_nodes
	
	::alloc_list
	storel 2		; [list]
	
	loadl 0			; [inode_data]
	loadl #dir_inode_store_offset
	ladd
	storel 4		; [subinode]
	
	:loop
	  ; We are at the end of the store
	  loadl 0		; [inode_data]
	  loadl #(#dir_inode_store_offset #dir_inode_store_length +)
	  ladd
	  loadl 4		; [subinode]
	  jeql :done
	  
	  ; There are no more nodes stored
	  loadl 4		; [subinode]
	  loadl [SP]
	  jzl :done
	  
	  loadl 2		; [list]
	  loadl 4		; [subinode]
	  loadl [SP]
	  ::list_add
	  
	  linc 4		; [subinode]
	  linc 4		; [subinode]
	jmp :loop
	:done
	
	loadl 0			; [inode_data]
	::inode_data_has_cont_node
	jz :no_cont
		
		; Add the inodes stored in the cont node
		derefl(0, #dir_cont_node_offset)
		loadl 2		; [list]
		::add_cont_node_inodes_to_list
		
	:no_cont
	loadl 0			; [inode_data]
	::free
	
	loadl 2			; [list]
	::list_to_array_free
	ret2

; Reserve and create an empty directory with the specified parent
:create_directory_inode
	4 4				; (/parent_ref w2, /name *)
	::reserve_inode
	ldup
	ldup
	
	loadl 0			; [parent_ref]
	loadl 2			; [name]
	::create_directory_inode_data_alloc
	
	::flush_and_free_inode
	
	loadl 0			; [parent_ref]
	lswap
	::add_inode_to_directory
	
	ret2
	
; Create the data for an empty directory inode
:create_directory_inode_data_alloc
	4 6				; (/parent_ref w2, /name *), data w*
	; Alloc node data
	load #inode_size
	::alloc_w
	storel 4		; [data]
	
	; Clear allocated data
	loadl 4			; [data]
	loadl #inode_size
	load #0
	::memset
	
	refset(4, #inode_type_directory)
	
	refsetl(4, #dir_parent_dir_offset, 0)
	
	loadl 2			; [name]
	linc linc
	loadl 4			; [data]
	loadl #dir_name_offset
	ladd
	
	loadl 2			; [name]
	loadl [SP]		; [name->length]
	loadl #dir_name_length
	::lmin
	
	; NOTE: Should we handle overflow in some way?
	memc			; Copy max 14 chars of the string
	
	loadl 4			; [data]
	ret2
	
; Adds one inode_ref to a directory
; FIXME: Check for a uniqe name!
; TODO: Check child type! (We cant add super or cont. nodes)
:add_inode_to_directory
	4 8				; (/parent_ref w2, /child_ref w2), p_data w*, index w2
	loadl 0			; [parent_ref]
	::load_inode_alloc
	storel 4		; [p_data]
	
	loadl #dir_inode_store_offset
	storel 6		; [index]
	
	; Find first empty slot
	:loop
	  loadl 4		; [p_data]
	  loadl 6		; [index]
	  ladd
	  loadl [SP]
	  jzl :found
	  linc 6		; [index]
	  linc 6		; [index]
	  loadl 6		; [index]
	  loadl #dir_cont_node_offset
	  jeql :expand
	jmp :loop
	:found
	loadl 4			; [p_data]
	loadl 6			; [index]
	ladd
	loadl 2			; [child_ref]
	storel [SP]
	
	loadl 0			; [parent_ref]
	loadl 4			; [p_data]
	::flush_and_free_inode
	ret
	
	:expand
	; FIXME: Implement
	; If needed allocate a new cont. node and add it to the current node.
	; Add the inode ref to the cont. node
	
	loadl 4			; [parent_ref]
	::inode_data_has_cont_node
	jnz :no_alloc
		
		; This adds the cont. node to the parent
		loadl 0		; [parent_ref]
		loadl 4		; [p_data]
		::create_cont_node_from_parent_data
		
		loadl 0		; [parent_ref]
		jnzl :flush
		
		derefl(4, #dir_cont_node_offset)
		
		; FIXME: This is very error prone! We need a better system
		; This is the super node
		; We need to reload it because last_block will have changed
		loadl 0		; [parent_ref]
		loadl 4		; [p_data]
		::load_inode
		
		loadl #dir_cont_node_offset
		ladd
		
		lswap
		
		storel [SP]	; [p_data.cont_node] = [new_cont_node]
		
		:flush
		; Flush the added cont. node
		loadl 0		; [parent_ref]
		loadl 4		; [p_data]
		::flush_inode
		jmp :add_inode
		
	:no_alloc
		
		derefl(4, #dir_cont_node_offset)
		
	:add_inode
	
	loadl 2			; [child_ref]
	::add_inode_to_cont_node
	
	; p_data will have been flushed if nessesary
	loadl 4			; [p_data]
	::free
	
	ret

:remove_inode_from_directory
	4 8				; (/parent_ref w2, /inode_ref w2), parent_data w2, index w2
	loadl 0			; [parent_ref]
	::load_inode_alloc
	storel 4		; [parent_data]
	
	loadl #0
	storel 6		; [index]
	
	:loop
	  
	  loadl 6		; [index]
	  loadl #dir_inode_store_length
	  jeql :no_inode
	
	  loadl 4		; [parent_data]
	  loadl #dir_inode_store_offset
	  loadl 6		; [index]
	  ladd
	  ladd
	  
	  loadl [SP]	; [parent_data.inode_store[index]]
	  loadl 2		; [inode_ref]
	  
	  jeql :found
	  
	  linc 6		; [index]
	  linc 6		; [index]
	  
	jmp :loop
	:found
	
	; Now we have the index
	; Shift the rest of the data down two steps
	
	loadl 4			; [parent_data]
	loadl #dir_inode_store_offset
	loadl 6			; [index]
	ladd
	ladd
	loadl #0
	storel [SP]		; [parent_data[index]] = 0
	
	loadl 4			; [parent_data]
	loadl #dir_inode_store_offset
	loadl 6			; [index]
	ladd
	ladd
	
	ldup
	
	loadl #2
	ladd			; [parent_data] + [index] + 2
	
	lswap
	
	loadl #dir_inode_store_length
	loadl 6			; [index]
	lsub
	
	memc			; Shift all later entries
	
	loadl 0			; [parent_ref]
	loadl 4			; [parent_data]
	::flush_and_free_inode
	
	ret
	
	:no_inode
	load "NO_INODE_IN_STORE "
	loadl 2			; [inode_ref]
	::w2_to_string_alloc
	::concat_string_alloc
	::panic_string
	
:get_dir_name_alloc
	2 4				; (/inode_ref w2), data w*
	loadl 0			; [inode_ref]
	::load_inode_alloc
	storel 2		; [data]
	
	load #16		; 14 chars + 2 length
	::alloc_w
	
	ldup
	loadl #14
	storel [SP]		; Store length in string
	
	loadl 2			; [data]
	loadl #dir_name_offset
	ladd
	
	lover
	linc linc		; Offset to data bit of string
	
	loadl #dir_name_length
	memc			; Copy 
	
	loadl 2			; [data]
	::free
	
	ret2			; Return the allocated string
	
:is_directory
	2 4				; (/inode_ref w2), data w*
	loadl 0			; [inode_ref]
	::load_inode_alloc
	storel 2		; [data]
	
	loadl 2			; [data]
	load [SP]		; [data->type]
	load #inode_type_directory
	jeq :yes
	
	loadl 2			; [data]
	load [SP]		; [data->type]
	load #inode_type_super
	jeq :yes
	
	:no
	loadl 2			; [data]
	::free
	load #false
	ret1
	
	:yes
	loadl 2			; [data]
	::free
	load #true
	ret1
	
	
	
	