
// FIXME: We need to include these so that the generic generation
// for the list types to work
// At the moment those procs are emitted in the scope of the using
// function, so if the generic function requires an import, so do we.
import StdUtil.t12;
import Console.t12;
import Heap.t12;

import StdString.t12;

import Map.t12;
import List.t12;

import OSL.t12;
import OSLParser.t12;
import OSLASTTypes.t12;

import Instructions.t12;

import TypeMap.t12;

struct TypeMap = Map<string, ASTType>;

[]word emit_code(*AST ast)
{
	*List<word> builder = alloc_list<word>(100);
	
	*TypeMap typeMap = cast(*TypeMap) alloc_map<string, ASTType>(10);
	
	*ASTFunction func = list_index<*ASTFunction>(&ast->Functions, 0);
	
	emit_function(builder, func);
	
	return list_to_array_free<word>(builder);
}

void emit_function(*List<word> instructions, *ASTFunction func)
{
	// Argument list!
	list_add<word>(instructions, 0);
	list_add<word>(instructions, 0);
	list_add<word>(instructions, Inst_Brk);
	
	dword length = func->Body.count;
	for (dword i = 0; i < length; i++) {
		emit_block_item(instructions, list_index<*ASTBlockItem>(&func->Body, i));
	}
}

void emit_block_item(*List<word> instructions, *ASTBlockItem blockItem)
{
	if (is_type_statement(blockItem->Node.Type))
	{
		emit_statement(instructions, cast(*ASTStatement) blockItem);
	}
	else
	{
		report_osl_error(Error, "Unknown block item type!");
	}
}

void emit_statement(*List<word> instructions, *ASTStatement statement)
{
	if (statement->BlockItem.Node.Type == typeof(ASTReturnStatement))
	{
		*ASTExpression retExpr = (cast(*ASTReturnStatement) statement)->ReturnValueExpression;
		if (retExpr != null)
		{
			emit_expression(instructions, retExpr);
			list_add<word>(instructions, Inst_Ret_1);
		}
		else
		{
			list_add<word>(instructions, Inst_Ret);
		}
	}
	else
	{
		brk();
		report_osl_error(Error, "Unknown statement type!");
	}
}

void emit_expression(*List<word> instructions, *ASTExpression expr)
{
	if (expr->Node.Type == typeof(ASTBinaryOp))
	{
		*ASTBinaryOp binOp = cast(*ASTBinaryOp) expr;
		
		emit_expression(instructions, binOp->Left);
		emit_expression(instructions, binOp->Right);
		// FIXME: Optype to instruction
		list_add<word>(instructions, binary_op_type_to_instruction(binOp->OpType));
	}
	else if (is_type_litteral(expr->Node.Type))
	{
		emit_litteral(instructions, cast(*ASTLitteral) expr);
	}
	else
	{
		report_osl_error(Error, format_string_alloc("Unknown expression: %s", cast(*void) get_type_name_alloc(expr->Node.Type)));
	}
}

void emit_litteral(*List<word> instructions, *ASTLitteral lit)
{
	if (lit->Expression.Node.Type == typeof(ASTNumericLitteral))
	{
		*ASTNumericLitteral numLit = cast(*ASTNumericLitteral) lit;
		
		if (numLit->IsDoubleWord)
		{
			list_add<word>(instructions, Inst_Load_lit_l);
			// FIXME: This
			// This is weird because the litteral is flipped in memory...
			list_add<word>(instructions, cast(word) (numLit->NumberValue / 0xFFF));
			list_add<word>(instructions, cast(word) (numLit->NumberValue & 0xFFF));
		}
		else
		{
			list_add<word>(instructions, Inst_Load_lit);
			list_add<word>(instructions, cast(word) numLit->NumberValue);
		}
	}
	else
	{
		report_osl_error(Error, format_string_alloc("Unknown litteral type: %s", cast(*void) get_type_name_alloc(get_type_from_id(lit->Expression.Node.Type))));
	}
}

