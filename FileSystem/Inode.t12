
import StdUtil.t12;
import Map.t12;

import Console.t12;

import Heap.t12;

import StdSort.t12;

public:

const word inode_size = 64;

// TODO: Enum types?
const word inode_type_unknown = 0;
const word inode_type_super = 1;
const word inode_type_file = 2;
const word inode_type_directory = 3;
const word inode_type_continuation = 4;

const word inode_type_offset = 0;
const word inode_cont_node_offset = 62;

const word inode_name_offset = 4;
const word inode_name_length = 14;

struct FSAddr = dword;

struct FSInode
{
	word type;
	// This is to pad the struct to 64 words
	[61]word data;
	FSAddr cont_node;
}

// This is works for FSFile and FSDirectory
struct FSNamedInode
{
	word type;
	word attrib;
	FSAddr parent_dir;
	[14]char name;
	[22]dword reserved;
	FSAddr cont_node;
}

struct FSInodeRef
{
	FSAddr address;
	*FSInode inode_data;
	dword refs;
	word flags;
}

global Map<FSAddr, *FSInodeRef> node_cache_map;

private:
global [64]Entry<FSAddr, *FSInodeRef> node_cache_map_array;
const word node_cache_map_size = 64;

public:

void init_inode_map()
{
	init_map_with_array<FSAddr, *FSInodeRef>(&node_cache_map, 
							cast(*Entry<FSAddr, *FSInodeRef>) &node_cache_map_array,
							node_cache_map_size);
}

*FSInodeRef rent_inode_ref(FSAddr address)
{
	*FSInodeRef inode_ref;
	if (map_try_get_value<FSAddr,*FSInodeRef>(&node_cache_map, address, &inode_ref) == false)
	{
		// Alloc and load a inode_ref
		inode_ref = alloc_inode_ref(address);
		
		// TODO: Fix the cast with generics
		map_add<FSAddr, *FSInodeRef>(&node_cache_map, address, inode_ref);
	}
	
	// If we have just alloced this it will be 0
	inode_ref->refs++;
	return inode_ref;
}

// FIXME: Check the flags on the ref to see if this is legal!
void flush_inode_ref(*FSInodeRef inode_ref)
{
	write(inode_ref->address, cast(*[64]word) inode_ref->inode_data);
}

// TODO: This should write the data!
void return_inode_ref(*FSInodeRef inode_ref)
{
	if (inode_ref == null)
		panic_string("RELEASED_NULL_INODE_REF");
	
	inode_ref->refs--;
	
	// This is to avoid weird sign problems
	// Would be <= otherwise
	if (inode_ref->refs == 0)
	{
		map_remove_key<FSAddr, *FSInodeRef>(&node_cache_map, inode_ref->address);
		inode_ref_free(inode_ref);
	}
}

*FSInodeRef alloc_inode_ref(FSAddr address)
{
	*FSInodeRef inode_ref = alloc_struct<FSInodeRef>();
	
	//print_string("Allocated inode ref: ");
	//print_w2_hex(cast(dword) inode_ref);
	//print_string(" addr: ");
	//print_w2_hex(cast(dword) address);
	//new_line();
	
	inode_ref->address = address;
	inode_ref->inode_data = load_inode_data_alloc(address);
	inode_ref->refs = 0;
	inode_ref->flags = 0;
	
	return inode_ref;
}

void inode_ref_free(*FSInodeRef inode_ref)
{
	//print_string("Freed inode ref: ");
	//print_w2_hex(cast(dword) inode_ref);
	//print_string(" addr: ");
	//print_w2_hex(cast(dword) inode_ref->address);
	//new_line();
	
	if (inode_ref->inode_data != null)
		free(inode_ref->inode_data);
		
	free(inode_ref);
}

*FSInode load_inode_data_alloc(FSAddr address)
{
	// NOTE: We might want some kind of way of 
	// detecting that *FSInode and *[64]word are compatible types
	*FSInode inode = alloc_struct<FSInode>();
	
	read(address, cast(*[64]word) inode);
	
	return inode;
}

// FIXME: This is really inefficient! We should
// only compare already loaded inodes!
word fsaddr_default_named_compare(FSAddr a1, FSAddr a2)
{
	*FSInodeRef ref1 = rent_inode_ref(a1);
	*FSInodeRef ref2 = rent_inode_ref(a2);
	*FSNamedInode a = cast(*FSNamedInode)ref1->inode_data;
	*FSNamedInode b = cast(*FSNamedInode)ref2->inode_data;
	
	if (a->type == inode_type_directory &&
		b->type == inode_type_file)
		return -1;
	else if (a->type == inode_type_file &&
		b->type == inode_type_directory)
		return 1;
	
	word res = F14char_default_compare(&a->name, &b->name);
	
	return_inode_ref(ref1);
	return_inode_ref(ref2);
	
	return res;
}

word F14char_default_compare(*[14]char a, *[14]char b)
{
	for (dword i = 0; i < a->length; i++)
	{
		word c;
		if ((c = (a[0][i] - b[0][i])) != 0)
			return c;
	}
	
	return 0;
}

intrinsic void read(FSAddr address, *FSInode data)
{
	"loadl #1"
	"read"
}


intrinsic void read(FSAddr address, *[64]word data)
{
	"loadl #1"
	"read"
}

intrinsic void write(FSAddr address, *FSInode data)
{
	"loadl #1"
	"write"
}

intrinsic void write(FSAddr address, *[64]word data)
{
	"loadl #1"
	"write"
}
